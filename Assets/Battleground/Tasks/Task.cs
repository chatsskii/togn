﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class Task
{
    private bool _is_complete = false;
    private bool _is_activated = false;
    protected List<Task> _waiting_tasks = new List<Task>();


    protected abstract IEnumerator _PerformanceTask();

    public bool IsComplete { get { return _is_complete; } }
    public bool IsActivated { get { return _is_activated; } }
    public bool IsReady
    { get
        {
            foreach (Task task in _waiting_tasks) { if (!task.IsComplete) return false; }
            return true;
        }
    }

    public void SetComplete() { _is_complete = true; }
    public void AddWaitingTask(Task task) { _waiting_tasks.Add(task); }

    protected void SetActivated() { _is_activated = true; }

    public IEnumerator PerformanceTask { get { return _PerformanceTask(); } }



}


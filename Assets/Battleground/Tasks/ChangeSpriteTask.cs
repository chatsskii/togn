﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ChangeSpriteTask : Task
{
    public string _sprite_name;
    public float _wait_time = 0;
    public BattleUnit _unit;
    public bool _return_sprite = false;

    public ChangeSpriteTask(BattleUnit unit, string sprite_name)
    {
        _unit = unit;
        _sprite_name = sprite_name;

    }

    public ChangeSpriteTask(BattleUnit unit, string sprite_name, float wait_time, bool return_sprite)
    {
        _unit = unit;
        _sprite_name = sprite_name;
        _wait_time = wait_time;
        _return_sprite = return_sprite;
    }


    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();
        Sprite previous_sprite = _unit.ScriptObject.GetComponent<SpriteRenderer>().sprite;
        _unit.ScriptObject.ChangeSprite(_sprite_name);
        Debug.LogWarning("Changing sprite :" + _sprite_name);
        yield return null;
        yield return new WaitForSeconds(_wait_time);
        if(_return_sprite) { _unit.ScriptObject.GetComponent<SpriteRenderer>().sprite = previous_sprite; }
        SetComplete();
        yield return null;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;


public class MoveToPositionTask : Task
{
    private Transform _transform;
    private Vector2 _target_position;
    private float _speed;
    private float _distance;

    private static float MOVE_DELTA = 0.1f;

    public MoveToPositionTask(Transform transform, Vector2 target, float speed)
    {
        _transform = transform;
        _target_position = target;
        _speed = speed;
        _distance = MOVE_DELTA;
    }

    public MoveToPositionTask(Transform transform, Vector2 target, float speed, float distance)
    {
        _transform = transform;
        _target_position = target;
        _speed = speed;
        _distance = distance + MOVE_DELTA;
    }

    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();

        while (Vector2.Distance(_target_position, _transform.position) > MOVE_DELTA + _distance)
        {
            _transform.position = Vector2.Lerp(_transform.position, _target_position, _speed * Time.deltaTime);
            yield return null;
        }

        SetComplete();
        yield return null;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class PushFromObjectTask : Task
{

    Transform _transform;
    Transform _from_transform;
    Vector2 _direction;
    float _speed;
    float _deceleration;

    public PushFromObjectTask(Transform transform, Transform from_transform, float start_speed, float deceleration)
    {
        _transform = transform;
        _from_transform = from_transform;
        _speed = start_speed;
        _deceleration = deceleration;
    }

    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();


        while (_speed > 0)
        {
            Vector2 position = _transform.position;
            Vector2 direction = (_transform.position - _from_transform.position).normalized;
            _transform.position = position + (direction * _speed * Time.deltaTime);

            _speed -= _deceleration * Time.deltaTime;

            yield return null;
        }

        SetComplete();

        yield return null;
    }
}


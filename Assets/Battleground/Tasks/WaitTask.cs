﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;


public class WaitTask : Task
{
    float _wait_time = 0f;

    public WaitTask() { }
    public WaitTask(float wait_time) { _wait_time = wait_time; }

    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();

        yield return new WaitForSeconds(_wait_time);
        SetComplete();
        yield return null;
    }
}


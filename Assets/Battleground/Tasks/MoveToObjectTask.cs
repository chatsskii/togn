﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using UnityEngine;


public class MoveToObjectTask : Task
{
    Transform _transform;
    Transform _target;
    float _speed;
    float _distance;

    private static float MOVE_DELTA = 0.1f;

    public MoveToObjectTask(Transform transform, Transform target, float speed, float distance)
    {
        _transform = transform;
        _target = target;
        _speed = speed;
        _distance = distance;
    }

    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();


        while (Vector2.Distance(_target.position, _transform.position) > MOVE_DELTA + _distance)
        {
            _transform.position = Vector2.Lerp(_transform.position, _target.position, _speed * Time.deltaTime);
            yield return null;
        }

        SetComplete();
        yield return null;
    }
}


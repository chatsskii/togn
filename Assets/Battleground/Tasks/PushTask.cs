﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class PushTask : Task
{

    Transform _transform;
    Vector2 _direction;
    float _speed;
    float _deceleration;

    public PushTask(Transform transform, Vector2 direction, float start_speed, float deceleration)
    {
        _transform = transform;
        _direction = direction.normalized;
        _speed = start_speed;
        _deceleration = deceleration;
    }

    protected override IEnumerator _PerformanceTask()
    {
        SetActivated();

        while (_speed > 0)
        {
            Vector2 position = _transform.position;
            _transform.position = position + (_direction * _speed * Time.deltaTime);

            _speed -= _deceleration * Time.deltaTime;

            yield return null;
        }

        SetComplete();

        yield return null;
    }
}


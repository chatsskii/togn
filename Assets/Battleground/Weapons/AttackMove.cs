﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class AttackMove
{
    private string _id;
    private float _multiplier;
    private int _count;
    private bool _is_massive = false;
    private bool _is_blockable = false;
    private bool _is_ignore_armor = false;

    private List<BattleAction> _on_use = new List<BattleAction>();
    private List<BattleAction> _on_hit = new List<BattleAction>();

    public string Id { get { return _id; } }
    public float Multiplier { get { return _multiplier; } }
    public int Count { get { return _count; } }
    public bool IsMassive { get { return _is_massive; } }
    public bool IsBlockable { get { return _is_blockable; } }
    public bool IsIgnoreArmor { get { return _is_ignore_armor; } }

    public AttackMove(string id, float multiplier, int count)
    {
        _id = id;
        _multiplier = multiplier;
        _count = count;
    }

    public void SetBlockable(bool is_blockable) { _is_blockable = is_blockable; }

    public void SetMassive(bool is_massive) { _is_massive = is_massive; }

    public void SetIgnoreArmor(bool is_ignore_armor) { _is_ignore_armor = is_ignore_armor; }

    public void Use(BattleUnit user, BattleUnit target)
    {

        int calculate_damage = (int)(user.Power * _multiplier);

        if (IsBlockable) { target.CheckTrigger(new BattleTrigger(TriggerType.OnBlock, target, user)); }

        if (user.Status != Status.Active) { return; }

        AttackPerformanceCollection.AddSingleById("P_BASIC_MELEE", calculate_damage, user, target);

        Debug.Log(String.Format("{0} attack with {1}({2}) {3}", user.SourceCharacter.Class.Id, _id, calculate_damage, target.SourceCharacter.Class.Id));
        target.TakeDamage(user, calculate_damage, false, _is_ignore_armor);

    }

    public void Use(BattleUnit user, List<BattleUnit> targets)
    {
        if (_is_massive)
        {
            int calculate_damage = (int)(user.Power * _multiplier);

            if (IsBlockable)
            {
                foreach (BattleUnit target in targets) { target.CheckTrigger(new BattleTrigger(TriggerType.OnBlock, target, user)); }
            }

            if (user.Status != Status.Active) { return; }

            for (int i = 0; i < Count; i++)
            {
                AttackPerformanceCollection.AddMassiveById("P_BASIC_MELEE", calculate_damage, user, targets);

                foreach (BattleUnit target in targets)
                {
                    Debug.Log(String.Format("{0} attack with {1}({2}) {3}", user.SourceCharacter.Class.Id, _id, calculate_damage, target.SourceCharacter.Class.Id));
                    target.TakeDamage(user, calculate_damage, false, _is_ignore_armor);
                } //On attack
            }
            



            // Weapon mass performance

            

        }
        else
        {
            foreach (BattleUnit unit in targets)
            {
                for (int i = 0; i < _count; i++)
                {
                    if (user.Status != Status.Active) { return; }
                    Use(user, unit);
                }
            }
        }
    }

}


﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

using UnityEngine;


public class UnitWeapon
{
    private string _id;

    private int _range;
    private int _max_count;

    private bool _include_allies = false;
    private bool _include_self = false;

    private List<AttackMove> _attacks = new List<AttackMove>();


    public UnitWeapon(string id, int range, int max_count, bool include_allies, bool include_self)
    {
        _id = id;
        _range = range;
        _max_count = max_count;
        _include_allies = include_allies;
        _include_self = include_self;
    }

    public string Id { get { return _id; } }

    public int Range { get { return _range; } }
    public int MaxCount { get { return _max_count; } }

    public bool IncludeAllies { get { return _include_allies; } }
    public bool IncludeSelf { get { return _include_self; } }

    public List<AttackMove> Attacks { get { return _attacks; } }
    public void AddAttack(AttackMove attack) { _attacks.Add(attack); }

    public List<BattleUnit> Use(BattleUnit user)
    {
        List<BattleUnit> targets = new List<BattleUnit>(SelectTargets(user));
        while (targets.Count > MaxCount)
        {
            int index = UnityEngine.Random.Range(0, targets.Count);          
            targets.RemoveAt(index);
        }

        foreach (AttackMove attack in Attacks) { attack.Use(user, targets); }

        return targets;
    }

    public void AttackTarget(BattleUnit user, BattleUnit target)
    {
        if (Battleground.UnitsInRange(user, Range, true).Contains(target))
        {
            foreach (AttackMove attack in Attacks) { attack.Use(user, target); }
        }
    }

    public List<BattleUnit> SelectTargets(BattleUnit user)
    {
        List<BattleUnit> units = Battleground.UnitsInRange(user, _range, false);
        units.RemoveAll((BattleUnit unit) =>
        {
            int distance = 0;
            if (unit.Team == user.Team) { distance = Math.Abs(unit.Position - user.Position); }
            else { distance = unit.Position + user.Position; }
            return distance > _range;
        });

        if (!_include_self) { units.Remove(user); }
        if (!_include_allies) { units.RemoveAll((BattleUnit unit) => { return (unit.Team == user.Team) && (unit != user) ; }); }

        return units;
    }

    public bool IfHaveTargets(BattleUnit user)
    {
        List<BattleUnit> units = SelectTargets(user);
        units.RemoveAll((BattleUnit unit) => { return unit.Team == user.Team; });
        return units.Count > 0;
    }

}


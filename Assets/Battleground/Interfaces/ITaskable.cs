﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public interface ITaskable
{
    void AddTask(Task task);
    void AddTask(Task task, bool wait_all_previous);
    List<Task> Tasks { get; }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public abstract class BattleEffect
{
    protected string _id;

    protected int _duration = 0;

    protected bool _is_permanent = true;
    private bool _first_tick = true;

    private BattleUnit _handler;
    private BattleUnit _inflictor;
    
    public BattleEffect(BattleUnit handler, BattleUnit inflictor)
    {
        _handler = handler;
        _inflictor = inflictor;
    }

    public void RemoveEffect()
    {
        OnEnd();
        Handler.RemoveEffect(this);
    }

    protected abstract void OnTick();
    protected abstract void OnStart();
    protected abstract void OnEnd();

    public void Tick()
    {
        if (_first_tick) { OnStart(); }

        OnTick();

        if (!IsPermanent)
        {
            _duration--;
            if (_duration == 0) { RemoveEffect(); }
        }
    }

    public string Id { get { return _id; } }
    public bool IsPermanent { get { return _is_permanent; } }
    public BattleUnit Handler { get { return _handler; } }
    public BattleUnit Inflictor { get { return _inflictor; } }
}


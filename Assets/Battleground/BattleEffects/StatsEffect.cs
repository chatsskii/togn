﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public abstract class StatsEffect : BattleEffect
{
    protected CalculationType _calculation;
    protected StatType _stat;
    protected float _value;

    public StatsEffect(BattleUnit handler, BattleUnit inflictor) : base(handler, inflictor) { }

    public float Value { get { return _value; } }
    public StatType Stat { get { return _stat; } }
    public CalculationType Calculation { get { return _calculation; } }
}


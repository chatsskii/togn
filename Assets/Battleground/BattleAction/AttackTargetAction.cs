﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class AttackTargetAction : BattleAction
{
    public override void Activate(Dictionary<string, object> values, BattleTrigger trigger)
    {
        BattleUnit user = (BattleUnit) values["USER"];
        BattleUnit target = (BattleUnit)values["TARGET"];
        user.Weapon.AttackTarget(user, target);
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public abstract class BattleAction
{
    private string _id;

    public string Id { get { return _id; } }

    public abstract void Activate(Dictionary<string, object> values, BattleTrigger trigger);
    
}


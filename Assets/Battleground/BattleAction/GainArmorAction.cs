﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class GainArmorAction : BattleAction
{
    public GainArmorAction() { }

    public override void Activate(Dictionary<string, object> values, BattleTrigger trigger)
    {
        int armor = (int)values["ARMOR"];
        BattleUnit user = (BattleUnit)values["USER"];

        user.TakeArmor(user, armor);
    }

}


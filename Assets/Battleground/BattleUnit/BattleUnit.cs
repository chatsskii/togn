﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BattleUnit
{
    private int _health;
    private int _morale;
    private int _armor;
    private int _position;

    private BattleTeam _team;
    private Status _status = Status.Active;

    private GameCharacter _source;
    private UnitWeapon _weapon;
    private UnitSpecial _special;
    private List<BattleAbility> _abilities = new List<BattleAbility>();
    private List<BattleEffect> _effects = new List<BattleEffect>();

    public BattleUnit(GameCharacter source, BattleTeam team)
    {
        _source = source;
        _team = team;

        _health = MaxHealth;
        _morale = 0;
        _armor = 0;
        _position = 3;
        _status = Status.Active;

        _weapon = _source.Class.BasicWeapon;

        foreach (BattleAbility ability in source.Class.BasicAbilities) { AddAbility(this, ability); }
        
    }


    private void TickEffects()
    {
        foreach(BattleEffect effect in _effects)
        {
            effect.Tick();
        }
    }
    private int CalculateStat(int base_value, StatType type)
    {
        int summation_value = 0;
        float multiplication_value = 1;
        int constant_value = 0;

        BattleUnit StrongestInflictor = null;


        foreach(StatsEffect effect in _effects)
        {
            if (effect.Stat != type) { continue; }

            switch (effect.Calculation)
            {
                case CalculationType.Summation:
                    summation_value += (int)effect.Value;
                    break;
                case CalculationType.Multiplication:
                    multiplication_value *= effect.Value;
                    break;
                case CalculationType.Constant:
                    if (StrongestInflictor == null)
                    {
                        StrongestInflictor = effect.Inflictor;
                        constant_value = (int)effect.Value;
                    }
                    else if(effect.Inflictor.Will > StrongestInflictor.Will)
                    {
                        StrongestInflictor = effect.Inflictor;
                        constant_value = (int)effect.Value;
                    }
                    break;
            }
        }

        if (StrongestInflictor == null) { return (int)((base_value + summation_value) * multiplication_value); }

        return constant_value;
    }


    public int Health { get { return _health; } }
    public int Morale { get { return _morale; } }
    public int Armor { get { return _armor; } }
    public int Position { get { return _position; } }

    public int MaxHealth { get { return Vitality * MainRules.HEALTH_CONSTANT; } }
    public int MaxMorale { get { return Will * MainRules.MORALE_CONSTANT; } }

    public int Vitality { get { return CalculateStat(_source.Vitality, StatType.Vitality); } }
    public int Power { get { return CalculateStat(_source.Power, StatType.Power); } }
    public int Will { get { return CalculateStat(_source.Will, StatType.Will); } }

    public int Speed { get { return CalculateStat(MainRules.STANDART_BATTLE_SPEED, StatType.Speed); } }

    public GameCharacter SourceCharacter { get { return _source; } }
    public UnitWeapon Weapon{ get { return _weapon; } }
    public UnitSpecial Special { get { return _special; } }
    public BattleTeam Team { get { return _team; } }
    public Status Status { get { return _status; } }
    public BattleTeam OppositeTeam { get { return (_team == BattleTeam.Computer) ? BattleTeam.Player : BattleTeam.Computer; } }
    public List<BattleAbility> Abilities { get { return _abilities; } }
    public List<BattleEffect> Effects { get { return _effects; } }
    public bool IsFleeing { get { return Morale < 0; }  }

    public UnitScript ScriptObject { get { return Battleground.Script.GetUnit(this); } }

    public void CheckTrigger(BattleTrigger trigger)
    {
        foreach (BattleAbility ability in Abilities) { ability.Activate(trigger); }
    }
    public void AddAbility(BattleUnit handler, BattleAbility ability)
    {
        ability.SetHandler(handler);
        _abilities.Add(ability);
    }

    public void UpdateArmor() { _armor = 0; }
    public bool Attack()
    {
        if (Weapon.IfHaveTargets(this))
        {
            List<BattleUnit> units = Weapon.Use(this);
            return true;
        }
        return false;
    }

    public bool Move()
    {
        int new_position = Math.Max(0, Position - Speed);
        if (new_position == _position) { return false; }
        ChangePosition(this, new_position, true);

        MoveToPositionTask move_task = ScriptObject.Move(new_position, 4f);
        ChangeSpriteTask idle_sprite = new ChangeSpriteTask(this, "IDLE");
        idle_sprite.AddWaitingTask(move_task);
        ScriptObject.AddTask(new ChangeSpriteTask(this, "RUN"));
        ScriptObject.AddTask(move_task);
        ScriptObject.AddTask(idle_sprite);

        return true;
    }
    public void StartCharge()
    {
        Battleground.Charge(this);
    }
    public void Retreat()
    {
        if(_position >= MainRules.ESCAPE_DISTANCE)
        {
            _status = Status.Escaped;
        }
        else
        {
            int new_position = Math.Max(0, Position + Speed);
            ChangePosition(this, new_position, true);
        }
    }

    public void TakeArmor(BattleUnit activator, int armor)
    {
        _armor += armor;
    }

    public void TakeDamage(BattleUnit activator, int damage, bool destroy_on_kill, bool ignore_armor)
    {
        if (ignore_armor) { _health -= damage; }
        else
        {
            int armor_damage = Math.Min(_armor, damage);
            int health_damage = damage - armor_damage;

            _armor = _armor - armor_damage;
            _health = _health - health_damage;

            CheckTrigger(new BattleTrigger(TriggerType.OnTakeDamage, activator, this));
        }

        Debug.Log(String.Format("{0} health: {1}/{2} + {3}", _source.Class.Id, _health, MaxHealth, Armor));

        if(_health <= 0 && _status == Status.Active)
        {
            Battleground.SetUnactive(this);

            CheckTrigger(new BattleTrigger(TriggerType.OnKill, this, activator));
            CheckTrigger(new BattleTrigger(TriggerType.OnDeath, activator, this));

            if (destroy_on_kill)
            {
                CheckTrigger(new BattleTrigger(TriggerType.OnDestroy, activator, this));
                _status = Status.Destroyed;
            }
            else
            {
                _status = Status.Dead;
            }
        }
    }

    public void InflictEffect(BattleEffect effect)
    {
        _effects.Add(effect);
    }
    public void RemoveEffect(BattleEffect effect)
    {
        _effects.Remove(effect);
    }

    public void ChangePosition(BattleUnit activator, int new_position, bool is_trigger)
    {
        if (new_position < 0) { new_position = 0; }

        Debug.Log(String.Format("{0}({1}): move from {2} to {3}", _source.Class.Id,(Team == BattleTeam.Player)?"Player":"Computer", _position, new_position));
        if (is_trigger) { CheckTrigger(new BattleTrigger(TriggerType.OnMove, activator, this)); }

        _position = new_position;
    }

    public void MakeTurn()
    {
        UpdateArmor();

        if (!Attack())
        {
            if (Position > 0) { Move(); }
            else { StartCharge(); }
        }

         if (Status == Status.Active) { ScriptObject.AddTask(ScriptObject.Move(_position, 10f), true); }

    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public abstract class BattleAbility
{
    protected string _id;

    protected TriggerType _trigger_type;
    private BattleUnit _handler = null;


    public string Id { get { return _id; } }
    public TriggerType Trigger { get { return _trigger_type; } }
    public BattleUnit Handler { get { return _handler; } }

    public void SetHandler(BattleUnit handler) { _handler = handler; }

    public void Activate(BattleTrigger trigger)
    {
        if (trigger.Type != Trigger) { return; }
        if (!Condition(trigger)) { return; }

        Action(trigger);
    }

    public abstract void Action(BattleTrigger trigger);
    public abstract bool Condition(BattleTrigger trigger);



}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class ShieldAbility : BattleAbility
{
    private int _armor = 0;

    public ShieldAbility(int armor)
    {
        _id = "BA_SHIELD";
        _trigger_type = TriggerType.OnBlock;
        _armor = armor;
    }

    public override void Action(BattleTrigger trigger)
    {
        Dictionary<string, object> values = new Dictionary<string, object>();
        values.Add("USER", Handler);
        values.Add("TARGET", Handler);
        values.Add("ARMOR", _armor);

        new GainArmorAction().Activate(values, trigger);
    }

    public override bool Condition(BattleTrigger trigger)
    {
        return trigger.User == Handler;
    }
}


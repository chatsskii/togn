﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class PreemptiveAttackAbility : BattleAbility
{
    public PreemptiveAttackAbility()
    {
        _id = "BA_PREEMPTIVE_ATTACK";
        _trigger_type = TriggerType.OnBlock;
    }


    public override void Action(BattleTrigger trigger)
    {
        Dictionary<string, object> values = new Dictionary<string, object>();
        values.Add("USER", Handler);
        values.Add("TARGET", trigger.Target);

        new AttackTargetAction().Activate(values, trigger);
    }

    public override bool Condition(BattleTrigger trigger)
    {
        if (trigger.User == Battleground.CurrentUnit) { return false; }
        if (trigger.User != Handler) { return false; }
        if (trigger.Target == Handler) { return false; }
        return true;
    }
}


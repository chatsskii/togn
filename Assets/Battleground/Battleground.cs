﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public static class Battleground
{
    private static BattlegroundScript _script;

    private static List<BattleUnit> _active_units;
    private static List<BattleUnit> _unactive_units;

    private static BattleUnit _current_unit;
    private static int _round;


    public static void InitalizeBattle(BattlegroundScript script, List<BattleUnit> units)
    {
        _script = script;

        _active_units = new List<BattleUnit>();
        _unactive_units = new List<BattleUnit>();

        foreach (BattleUnit unit in units) { SpawnUnit(unit, MainRules.START_POSITION); }



        _round = 0;
        _current_unit = null;
    }

    public static BattlegroundScript Script { get { return _script; } }

    public static int Round { get { return _round; } }
    public static List<BattleUnit> AllUnits
    {
        get
        {
            List<BattleUnit> units = new List<BattleUnit>();
            units.AddRange(ActiveUnits);
            units.AddRange(UnactiveUnits);

            return units;
        }
    }
    public static List<BattleUnit> ActiveUnits { get { return _active_units; } }
    public static List<BattleUnit> UnactiveUnits { get { return _unactive_units; } }

    public static List<BattleUnit> UnitsInRange(BattleUnit user, int range, bool include_unactive)
    {
        List<BattleUnit> units = new List<BattleUnit>();
        units.AddRange(ActiveUnits);
        if (include_unactive) { units.AddRange(UnactiveUnits); }
           
        units.RemoveAll((BattleUnit unit) => { return unit.Position + user.Position + ((user.Team == unit.Team) ? 0 : 1) > range; } );

        return units;       
    }
    public static BattleUnit CurrentUnit { get { return _current_unit; } }

    public static List<BattleUnit> GetUnits(BattleTeam team, bool include_unactive)
    {
        List<BattleUnit> units = new List<BattleUnit>();
        units.AddRange(ActiveUnits);
        if (include_unactive) { units.AddRange(UnactiveUnits); }
        units.RemoveAll((BattleUnit unit) => { return unit.Team != team; });

        return units;
    }
    public static List<BattleUnit> UnitsOnPosition(BattleTeam team, int position, bool include_unactive)
    {
        List<BattleUnit> units = GetUnits(team, include_unactive);
        units.RemoveAll((BattleUnit unit) => { return unit.Position != position; });
        return units;
    }

    public static int UnitsCount(bool include_unactive)
    {
        List<BattleUnit> units = (include_unactive) ? AllUnits : ActiveUnits;
        return units.Count;
    }
    public static int UnitsCount(BattleTeam team, bool include_unactive)
    {
        List<BattleUnit> units = GetUnits(team, include_unactive);

        return units.Count;
    }

    private static void SortActiveUnits()
    {
        _active_units.Sort((BattleUnit unit1, BattleUnit unit2) => { return unit2.Power - unit1.Power; });
    }


    public static void SetUnactive(BattleUnit unit)
    {
        Debug.Log("Is Inactive");
        if(_active_units.Remove(unit))
        {
            _unactive_units.Add(unit);
        }
    }


    public static void StartBattle()
    {
        _round = 0;

        Debug.Log("Start battle");
        Debug.Log("Count: " + _active_units.Count);
        Debug.Log("Player count: " + UnitsCount(BattleTeam.Player, false));
        Debug.Log("Computer count: " + UnitsCount(BattleTeam.Computer, false));



        while (_round < 50 && UnitsCount(BattleTeam.Player, false) > 0 && UnitsCount(BattleTeam.Computer, false) > 0)
        {
            SortActiveUnits();
            int unit_counter = 0;
            Debug.Log("Round: " + _round);

            WaitTask start_round = new WaitTask();
            WaitTask end_round = new WaitTask();


            while (unit_counter < _active_units.Count)
            {
                _current_unit = _active_units[unit_counter];
                UnitScript current_script = Script.GetUnit(_current_unit);

                WaitTask start_turn = new WaitTask();
                WaitTask end_turn = new WaitTask();


                start_turn.AddWaitingTask(start_round);

                current_script.AddTask(start_turn);
                _current_unit.MakeTurn();
                current_script.AddTask(end_turn, true);

                end_round.AddWaitingTask(end_turn);



                unit_counter++;
            }

            AddTask(start_round);
            AddTask(end_round);




            _round++;
        }

        Debug.Log("Player count: " + UnitsCount(BattleTeam.Player, false));
        Debug.Log("Computer count: " + UnitsCount(BattleTeam.Computer, false));
        Debug.Log("End battle");
    }
    public static void Charge(BattleUnit charger)
    {
        while(UnitsOnPosition(charger.OppositeTeam, 0, false).Count == 0 && UnitsCount(charger.OppositeTeam, false) > 0)
        {
            foreach (BattleUnit enemy in GetUnits(charger.OppositeTeam, false))
            {
                enemy.ChangePosition(enemy, enemy.Position - 1, false);
            }

            foreach(BattleUnit ally in GetUnits(charger.Team, true))
            {
                if (ally.IsFleeing || ally.Status != Status.Active) { ally.ChangePosition(ally, ally.Position + 1, false); }
                else { ally.ChangePosition(ally, ally.Position, true); }
            }
        }

    }

    public static void SpawnUnit(BattleUnit unit, int position)
    {
        unit.ChangePosition(unit, position, false);

        if (unit.Status == Status.Active) { _active_units.Add(unit); }
        else { _unactive_units.Add(unit); }

        Script.SpawnUnit(unit);
    }
    public static void AddTask(Task task) { Script.AddTask(task); }
}


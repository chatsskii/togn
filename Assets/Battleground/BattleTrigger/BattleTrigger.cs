﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class BattleTrigger
{

    TriggerType _type;

    BattleUnit _user;
    BattleUnit _target;

    Dictionary<string, object> _values = new Dictionary<string, object>();

    public BattleTrigger(TriggerType type, BattleUnit user, BattleUnit target)
    {
        _type = type;
        _user = user;
        _target = target;
    }

    public TriggerType Type { get { return _type; } }
    public BattleUnit User { get { return _user; } }
    public BattleUnit Target { get { return _target; } }
    public object GetValue(string name) { return _values[name]; }

}


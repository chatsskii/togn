﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public enum Status
{
    Active = 0,
    Escaped,
    Dead,
    Destroyed
}


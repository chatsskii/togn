﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public enum TriggerType
{
    OnMove = 0,
    OnBlock,
    OnTakeDamage,
    OnDeath,
    OnDestroy,
    OnKill
}


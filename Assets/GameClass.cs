﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class GameClass
{
    private string _id;
    private int _level;
    private int _max_artifacts;

    private int _base_vitality = 0;
    private int _base_power = 0;
    private int _base_will = 0;

    private int _bonus_vitality = 0;
    private int _bonus_power = 0;
    private int _bonus_will = 0;

    private UnitWeapon _basic_weapon= null;
    private UnitSpecial _basic_special = null;

    private List<BattleAbility> _basic_abilities = new List<BattleAbility>();

    private SpritesContainer _sprites = SpritesCollection.GetContainer("S_PEASANT");

    public string Id { get { return _id; } }
    public int Level { get { return _level; } }
    public int MaxArtifacts { get { return _max_artifacts; } }

    public int BaseVitality { get { return _base_vitality; } }
    public int BasePower { get { return _base_power; } }
    public int BaseWill { get { return _base_will; } }

    public int BonusVitality { get { return _bonus_vitality; } }
    public int BonusPower { get { return _bonus_power; } }
    public int BonusWill { get { return _bonus_will; } }

    public UnitWeapon BasicWeapon{ get { return _basic_weapon; } }
    public UnitSpecial BasicSpecial { get { return _basic_special; } }

    public UnityEngine.Sprite GetSprite(string name) { return _sprites.GetSprite(name); }

    public List<BattleAbility> BasicAbilities { get { return _basic_abilities; } } 

    public GameClass(string id, int level, int max_artifacts)
    {
        _id = id;
        _level = level;
        _max_artifacts = max_artifacts;
    }

    public void SetBaseStats(int vitality, int power, int will)
    {
        _base_vitality = vitality;
        _base_power = power;
        _base_will = will;
    }

    public void SetBonusStats(int vitality, int power, int will)
    {
        _bonus_vitality = vitality;
        _bonus_power = power;
        _bonus_will = will;
    }

    public void SetBasicWeapon(UnitWeapon weapon)
    {
        _basic_weapon= weapon;
    }

    public void SetupBasicSpecial(UnitSpecial special)
    {
        _basic_special = special;
    }

    public void AddBasicAbility(BattleAbility ability)
    {
        _basic_abilities.Add(ability);
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitScript : MonoBehaviour, ITaskable
{

    private BattleUnit _unit;

    private List<Task> _performance_tasks = new List<Task>();

    void Start ()
    {
        
    }

    public BattleUnit Unit { get { return _unit; } }

    public void InitializeUnit(BattleUnit unit)
    {
        _unit = unit;
        ChangeSprite("IDLE");
        if (Unit.Team == BattleTeam.Computer) { GetComponent<SpriteRenderer>().flipX = true; }
        StartCoroutine(TaskRoutine());
    }

    public List<Task> Tasks { get { return _performance_tasks; } }
    private IEnumerator TaskRoutine()
    {
        while (true)
        {
            for(int i = 0; i < _performance_tasks.Count; i++)
            {
                if (_performance_tasks[i].IsComplete) { _performance_tasks.RemoveAt(i); break; }
                if (_performance_tasks[i].IsReady && !_performance_tasks[i].IsActivated) { StartCoroutine(_performance_tasks[i].PerformanceTask); }
                if (!_performance_tasks[i].IsReady) { break; }
            }

            yield return null;
        }
    }

    public void AddTask(Task task) { AddTask(task, false); }

    public void AddTask(Task task, bool wait_all_previous)
    {
        if (wait_all_previous)
        {
            foreach (Task crr_task in Tasks) { task.AddWaitingTask(crr_task); }
        }

        _performance_tasks.Add(task);
    }

    public MoveToPositionTask Move(Vector2 target, float speed, float distance)
    {
        return new MoveToPositionTask(transform, target, speed, distance);
    }
    public MoveToPositionTask Move(Vector2 target, float speed)
    {
        return Move(target, speed, 0);
    }
    public MoveToPositionTask Move(int position, float speed)
    {
        float x_position = Battleground.Script.Center.x + Unit.Position * Battleground.Script.DISTANCE_VALUE * ((Unit.Team == BattleTeam.Player)?  -1 : 1);
        float y_position = Battleground.Script.Center.y - Battleground.Script.FIELD_WIDTH / 2 + Random.Range(0, Battleground.Script.FIELD_WIDTH);

        return Move(new Vector2(x_position, y_position), speed);
    }
    public MoveToObjectTask Move(BattleUnit unit, float speed, float distance)
    {
        Vector2 position = Battleground.Script.GetUnit(unit).transform.position;

        return new MoveToObjectTask(transform, unit.ScriptObject.transform, speed, distance);
    }
    public void ChangeSprite(string sprite_name)
    {
        GetComponent<SpriteRenderer>().sprite = Unit.SourceCharacter.Class.GetSprite(sprite_name);
    }

    public PushTask Push(Vector2 direction, float start_speed, float deceleration)
    {
        return new PushTask(transform, direction, start_speed, deceleration);
    }

    public PushFromObjectTask Push(BattleUnit from_unit, float start_speed, float deceleration)
    {
        return new PushFromObjectTask(transform, from_unit.ScriptObject.transform, start_speed, deceleration);
    }

}

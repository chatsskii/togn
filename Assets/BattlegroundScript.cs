﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattlegroundScript : MonoBehaviour, ITaskable
{

    [SerializeField]
    UnitScript unit_prefab;

    [SerializeField]
    public float DISTANCE_VALUE = 1f;

    [SerializeField]
    public float FIELD_WIDTH = 5f;

    private Dictionary<BattleUnit, UnitScript> _units_collection = new Dictionary<BattleUnit, UnitScript>();
    private List<Task> _performance_tasks = new List<Task>();

	void Start()
    {

        SpritesCollection.LoadCollection();
        ClassCollection.LoadCollection();


        GameClass peasant = ClassCollection.GetById("CL_PEASANT");
        GameClass militia = ClassCollection.GetById("CL_MILITIA");
        GameClass sword_master = ClassCollection.GetById("CL_SWORD_MASTER");
        GameClass berserker = ClassCollection.GetById("CL_BERSERKER");


        List<BattleUnit> units = new List<BattleUnit>();


        //units.Add(new BattleUnit(new GameCharacter(sword_master), BattleTeam.Player));

        for (int i = 0; i < 4; i++)
        {
            //units.Add(new BattleUnit(new GameCharacter(sword_master), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(militia), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(militia), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Computer));
            units.Add(new BattleUnit(new GameCharacter(berserker), BattleTeam.Computer));

            //units.Add(new BattleUnit(new GameCharacter(sword_master), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(militia), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(militia), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(peasant), BattleTeam.Player));
            units.Add(new BattleUnit(new GameCharacter(berserker), BattleTeam.Player));

        }


        Battleground.InitalizeBattle(this, units);
        Battleground.StartBattle();

        StartCoroutine(TaskRoutine());
    }

    public Vector3 Center { get { return transform.position; }  }
    public float RigthBorder { get { return transform.position.x + MainRules.ESCAPE_DISTANCE * DISTANCE_VALUE;  } }
    public float LeftBorder { get { return transform.position.x - MainRules.ESCAPE_DISTANCE * DISTANCE_VALUE; } }

    public List<Task> Tasks { get { return _performance_tasks; } }
    private IEnumerator TaskRoutine()
    {
        while (true)
        {
            for (int i = 0; i < _performance_tasks.Count; i++)
            {
                if (_performance_tasks[i].IsComplete) { _performance_tasks.RemoveAt(i); break; }
                if (_performance_tasks[i].IsReady && !_performance_tasks[i].IsActivated) { StartCoroutine(_performance_tasks[i].PerformanceTask); }
                if (!_performance_tasks[i].IsReady) { break; }
            }

            yield return null;
        }
    }

    public void AddTask(Task task) { AddTask(task, false); }

    public void AddTask(Task task, bool wait_all_previous)
    {
        if(wait_all_previous)
        {
            foreach (Task crr_task in Tasks) { task.AddWaitingTask(crr_task); }
        }

        _performance_tasks.Add(task);
    }

    public UnitScript GetUnit(BattleUnit unit) { return _units_collection[unit]; }

    public void SpawnUnit(BattleUnit unit)
    {
        GameObject unit_object = Instantiate(unit_prefab.gameObject);
        UnitScript script = unit_object.GetComponent<UnitScript>();
        _units_collection.Add(unit, script);

        script.InitializeUnit(unit);

        float x_position = (unit.Team == BattleTeam.Player) ? LeftBorder : RigthBorder;
        float y_position = Random.Range(0, FIELD_WIDTH) - FIELD_WIDTH / 2;
        unit_object.transform.position = new Vector3(x_position, y_position, 0);
    }
	

}

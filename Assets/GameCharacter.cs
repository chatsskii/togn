﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class GameCharacter
{
    private GameClass _source_class;

    private int _vitality;
    private int _power;
    private int _will;

    public GameCharacter(GameClass source_class)
    {
        _source_class = source_class;
        _vitality = source_class.BaseVitality;
        _power = source_class.BasePower;
        _will = source_class.BaseWill;
    }

    public GameClass Class { get { return _source_class; } }

    public int Vitality { get { return _vitality; } }
    public int Power { get { return _power; } }
    public int Will { get { return _will; } }
}


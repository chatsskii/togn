﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Networking;

public class SpriteScript : MonoBehaviour {

    private SpriteRenderer _sprite_renderer;


    private string IMAGE_PATH = Directory.GetCurrentDirectory() + "\\Textures\\test.png";

	void Start ()
    {
        Debug.LogWarning(IMAGE_PATH);
        _sprite_renderer = GetComponent<SpriteRenderer>();

        StartCoroutine(GetSprite(_sprite_renderer, IMAGE_PATH));
    }

    IEnumerator GetSprite(SpriteRenderer renderer, string path)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(IMAGE_PATH);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError) { Debug.Log(request.error); }
        else
        {
            Texture2D texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
            renderer.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

        }

    }
	
	// Update is called once per frame
	void Update ()
    {
        
    }
}

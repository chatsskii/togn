﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public static class MainRules
{
    public static int STANDART_BATTLE_SPEED = 1;
    public static int HEALTH_CONSTANT = 5;
    public static int MORALE_CONSTANT = 5;

    public static int ESCAPE_DISTANCE = 8;
    public static int START_POSITION = 5;
}


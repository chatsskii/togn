﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class SpritesContainer
{
    private string _id;
    private Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();

    public SpritesContainer(string id)
    {
        _id = id;
        _sprites = new Dictionary<string, Sprite>();
    }

    public string Id { get { return _id; } }

    public void AddSprite(string name, Sprite sprite) { _sprites.Add(name, sprite); }
    public Sprite GetSprite(string name) { return _sprites[name]; }
    
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public static class AttackPerformanceCollection
{
    public static float STANDART_ATTACK_SPEED = 10f;
    public static float STANDART_PUSH_SPEED = 6f;
    public static float STANDART_PUSH_DECELERATION = 30f;


    public static void AddSingleById(string id, int damage, BattleUnit user, BattleUnit target)
    {
        switch (id)
        {
            case ("P_BASIC_MELEE"):

                float move_speed = STANDART_ATTACK_SPEED;
                float push_speed = STANDART_PUSH_SPEED * (0.7f + damage / target.MaxHealth);
                float push_stop = STANDART_PUSH_DECELERATION;
                float distance = user.Weapon.Range * Battleground.Script.DISTANCE_VALUE * 0.2f;

                WaitTask wait_before = new WaitTask();
                MoveToObjectTask move_task = user.ScriptObject.Move(target, move_speed, distance);
                PushFromObjectTask push_after_attack = target.ScriptObject.Push(user, push_speed, push_stop);
                WaitTask wait_after = new WaitTask();

                move_task.AddWaitingTask(wait_before);
                push_after_attack.AddWaitingTask(move_task);

                target.ScriptObject.AddTask(wait_before, true);
                user.ScriptObject.AddTask(move_task, true);
                target.ScriptObject.AddTask(push_after_attack);
                target.ScriptObject.AddTask(wait_after, true);

                break;

            default:
                throw new Exception("Single performance " + id + " not found!");
        }

    }

    public static void AddMassiveById(string id, int damage, BattleUnit user, List<BattleUnit> targets)
    {
        switch (id)
        {
            case ("P_BASIC_MELEE"):
                float move_speed = STANDART_ATTACK_SPEED;
                float push_stop = STANDART_PUSH_DECELERATION;
                float distance = user.Weapon.Range * Battleground.Script.DISTANCE_VALUE * 0.1f;


                MoveToPositionTask user_move_task = user.ScriptObject.Move(Battleground.Script.Center, move_speed);
                WaitTask wait_attack = new WaitTask();
                WaitTask user_wait_after = new WaitTask();

                user.ScriptObject.AddTask(user_move_task, true);
                user.ScriptObject.AddTask(wait_attack, true);
                user.ScriptObject.AddTask(user_wait_after, true);

                foreach(BattleUnit target in targets)
                {
                    float push_speed = STANDART_PUSH_SPEED * (1 + damage / target.MaxHealth);

                    WaitTask wait_before = new WaitTask();
                    MoveToObjectTask target_move = target.ScriptObject.Move(user, move_speed / 2, distance);
                    PushFromObjectTask target_push = target.ScriptObject.Push(user, push_speed, push_stop);
                    WaitTask wait_after = new WaitTask();

                    user_move_task.AddWaitingTask(wait_before);
                    target_move.AddWaitingTask(user_move_task);
                    wait_attack.AddWaitingTask(target_move);
                    target_push.AddWaitingTask(wait_attack);
                    user_wait_after.AddWaitingTask(wait_after);

                    target.ScriptObject.AddTask(wait_before, true);
                    target.ScriptObject.AddTask(target_move);
                    target.ScriptObject.AddTask(wait_attack, true);
                    target.ScriptObject.AddTask(target_push);
                    target.ScriptObject.AddTask(wait_after, true);
                }

                break;

            default:
                throw new Exception("Massive performance " + id + " not found!");
        }
    }

}


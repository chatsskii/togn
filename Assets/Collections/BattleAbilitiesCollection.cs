﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;


public static class BattleAbilitiesCollection
{
    public static BattleAbility GetByXml(XmlNode node)
    {
        string id = node.InnerText;

        switch(id)
        {
            case ("BA_SHIELD"):
                int armor = Convert.ToInt32(node.Attributes["ARMOR"].Value);
                return new ShieldAbility(armor);
            case ("BA_PREEMPTIVE_ATTACK"):
                return new PreemptiveAttackAbility();
                
        }


        throw new Exception("Ability " + id + " not found!");
    }
}


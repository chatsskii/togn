﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.IO;
using System.Xml;


public static class SpritesCollection
{
    private static Dictionary<string, SpritesContainer> _containers = new Dictionary<string, SpritesContainer>();

    private static string SPRITES_FOLDER = Directory.GetCurrentDirectory() + "\\Collections\\Sprites";

    private static Sprite LoadSprite(string path, int width, int heigth)
    {

        byte[] image_data = File.ReadAllBytes(path);

        Texture2D texture = new Texture2D(width, heigth);
        texture.filterMode = FilterMode.Point;
        texture.LoadImage(image_data);

        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

        return sprite;
    }


    public static SpritesContainer GetContainer(string name) { return _containers[name]; }
    public static void LoadCollection()
    {
        _containers = new Dictionary<string, SpritesContainer>();

        string[] sprite_folders = Directory.GetDirectories(SPRITES_FOLDER);

        foreach(string folder in sprite_folders)
        {
            if (!File.Exists(folder + "\\SpritesConfig.xml")) { break; }

            XmlDocument config_document = new XmlDocument();
            config_document.Load(folder + "\\SpritesConfig.xml");

            XmlNode config_node = config_document.SelectSingleNode("SpritesConfig");
            if (config_node == null) { continue; }

            string id = config_node.Attributes["id"].Value;

            SpritesContainer adding_container = new SpritesContainer(id);

            foreach(XmlNode node in config_node.SelectNodes("Sprites/Sprite"))
            {
                string sprite_id = node.Attributes["id"].Value;
                string file_name = node.SelectSingleNode("FileName").InnerText;
                int width = Convert.ToInt32(node.SelectSingleNode("Width").InnerText);
                int heigth = Convert.ToInt32(node.SelectSingleNode("Heigth").InnerText);

                Sprite sprite = LoadSprite(folder + "\\" + file_name, width, heigth);

                adding_container.AddSprite(sprite_id, sprite);
            }

            _containers.Add(id, adding_container);
        }
    }



}


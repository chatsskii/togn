﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using UnityEngine;


public static class ClassCollection
{
    public static Dictionary<string, GameClass> _class_dictionary = new Dictionary<string, GameClass>();

    private static string CLASS_FOLDER = Directory.GetCurrentDirectory() + "\\Collections\\Classes";

    public static void LoadCollection()
    {
        _class_dictionary = new Dictionary<string, GameClass>();

        string[] class_files = Directory.GetFiles(CLASS_FOLDER,"CL_*.xml");

        foreach(string file in class_files)
        {

            XmlDocument class_document = new XmlDocument();
            class_document.Load(file);
            XmlNode class_node = class_document.SelectSingleNode("Class");
            if (class_node == null) { continue; }

            string id = class_node.Attributes["id"].Value;
            int level = Convert.ToInt32(class_node.SelectSingleNode("ClassLevel").InnerText);
            int max_artifacts = Convert.ToInt32(class_node.SelectSingleNode("MaxArtifacts").InnerText);

            int base_vitality = Convert.ToInt32(class_node.SelectSingleNode("Stats/Base/Vitality").InnerText);
            int base_power = Convert.ToInt32(class_node.SelectSingleNode("Stats/Base/Power").InnerText);
            int base_will = Convert.ToInt32(class_node.SelectSingleNode("Stats/Base/Will").InnerText);

            int bonus_vitality = Convert.ToInt32(class_node.SelectSingleNode("Stats/Bonus/Vitality").InnerText);
            int bonus_power = Convert.ToInt32(class_node.SelectSingleNode("Stats/Bonus/Power").InnerText);
            int bonus_will = Convert.ToInt32(class_node.SelectSingleNode("Stats/Bonus/Will").InnerText);

            string weapon_id = class_node.SelectSingleNode("BasicWeapon").InnerText;
            UnitWeapon weapon = WeaponsCollection.GetById(weapon_id);
            if (weapon == null)
            {
                WeaponsCollection.LoadCollection();
                weapon = WeaponsCollection.GetById(weapon_id);
                if (weapon == null) { throw new Exception(weapon_id + " NOT FOUND!"); }
            }

            GameClass adding_class = new GameClass(id, level, max_artifacts);
            adding_class.SetBaseStats(base_vitality, base_power, base_will);
            adding_class.SetBonusStats(bonus_vitality, bonus_power, bonus_will);
            adding_class.SetBasicWeapon(weapon);

            foreach(XmlNode node in class_node.SelectNodes("Abilities/Ability"))
            {
                adding_class.AddBasicAbility(BattleAbilitiesCollection.GetByXml(node));
            }


            _class_dictionary.Add(id, adding_class);

        }

    }

    private static Dictionary<string, GameClass> Dictionary { get { return _class_dictionary; } }

    public static GameClass GetById(string id)
    {
        GameClass game_class = null;
        Dictionary.TryGetValue(id, out game_class);
        return game_class;
    }

}


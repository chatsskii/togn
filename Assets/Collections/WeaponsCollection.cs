﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;


public static class WeaponsCollection
{
    public static Dictionary<string, UnitWeapon> _weapons_dictionary = new Dictionary<string, UnitWeapon>();

    private static string WEAPONS_FOLDER = Directory.GetCurrentDirectory() + "\\Collections\\Weapons";

    public static void LoadCollection()
    {
        _weapons_dictionary = new Dictionary<string, UnitWeapon>();

        string[] weapons_files = Directory.GetFiles(WEAPONS_FOLDER, "WP_*.xml");

        foreach (string file in weapons_files)
        {
            XmlDocument weapon_document = new XmlDocument();
            weapon_document.Load(file);
            XmlNode weapon_node = weapon_document.SelectSingleNode("Weapon");
            if (weapon_node == null) { continue; }

            string id = weapon_node.Attributes["id"].Value;

            int range = Convert.ToInt32(weapon_node.SelectSingleNode("Range").InnerText);
            int max_count = Convert.ToInt32(weapon_node.SelectSingleNode("MaxCount").InnerText);
            bool include_allies = (weapon_node.SelectSingleNode("IncludeAllies").InnerText == "true") ? true : false;
            bool include_self = (weapon_node.SelectSingleNode("IncludeSelf").InnerText == "true") ? true : false;

            UnitWeapon weapon = new UnitWeapon(id, range, max_count, include_allies, include_self);

            foreach (XmlNode node in weapon_node.SelectNodes("Attacks/Attack"))
            {
                int count = Convert.ToInt32(node.Attributes["count"].Value);
                string attack_id = node.InnerText;
                for (int i = 0; i < count; i++)
                {
                   AttackMove attack = AttacksCollection.GetById(attack_id);

                    if(attack == null)
                    {
                        AttacksCollection.LoadCollection();
                        attack = AttacksCollection.GetById(attack_id);
                        if (attack == null) { throw new Exception(attack_id + " NOT FOUND!"); }
                    }
                    weapon.AddAttack(attack);
                }
            }

            _weapons_dictionary.Add(id, weapon);
        }

    }

    private static Dictionary<string, UnitWeapon> Dictionary { get { return _weapons_dictionary; } }

    public static UnitWeapon GetById(string id)
    {
        UnitWeapon weapon = null;
        Dictionary.TryGetValue(id, out weapon);
        return weapon;
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Xml;
using System.IO;

using UnityEngine;

public static class AttacksCollection
{
    private static string ATTACKS_FOLDER = Directory.GetCurrentDirectory() + "\\Collections\\Attacks";

    private static Dictionary<string, AttackMove> _attacks_dictionary = new Dictionary<string, AttackMove>();

    private static Dictionary<string, AttackMove> Dictionary { get { return _attacks_dictionary; } }

    public static void LoadCollection()
    {
        _attacks_dictionary = new Dictionary<string, AttackMove>();

        string[] attack_files = Directory.GetFiles(ATTACKS_FOLDER, "AT_*.xml");

        foreach(string file in attack_files)
        {
            XmlDocument attack_document = new XmlDocument();
            attack_document.Load(file);
            XmlNode attack_node = attack_document.SelectSingleNode("Attack");
            if (attack_node == null) { continue; }

            string id = attack_node.Attributes["id"].Value;
            Debug.Log(id);

            float damage_multiplier = (float)Convert.ToDouble(attack_node.SelectSingleNode("DamageMultiplier").InnerText);
            int count = Convert.ToInt32(attack_node.SelectSingleNode("Count").InnerText);
            bool is_blockable = (attack_node.SelectSingleNode("IsBlockable").InnerText == "true") ? true : false;
            bool is_massive = (attack_node.SelectSingleNode("IsMassive").InnerText == "true") ? true : false;
            bool is_ignore_armor = (attack_node.SelectSingleNode("IsIgnoreArmor").InnerText == "true") ? true : false;

            AttackMove adding_attack = new AttackMove(id, damage_multiplier, count);
            adding_attack.SetBlockable(is_blockable);
            adding_attack.SetMassive(is_massive);
            adding_attack.SetIgnoreArmor(is_ignore_armor);

            _attacks_dictionary.Add(id, adding_attack);


        }
    }

    public static AttackMove GetById(string id)
    {
        AttackMove attack = null;
        Dictionary.TryGetValue(id, out attack);
        return attack;
    } 

}
